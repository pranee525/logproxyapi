# LogProxyApi
A project to save the log proxy information has been created as per the instructions

Assumptions:
•	The authentication information is stored in a list with two users (In a real world scenario this information will be stored in the database).
•	User Information username: test password:test,username: test_sample,password: test
•	Used basic authentication(username,password)
•	Api contains two methods Get and post


Api Paths and Parameters
•	Api Path(get) http://localhost:5000/logproxy/GetLog
    1. 	Parameters – maxRecords(int)
•	Api Path(post): http://localhost:5000/logproxy/PostLog
    1.	Parameters – text(string) – contains the summary of the log message
    2.	Parameters – title(string) – contains the title of the log message
•	Pass basic authentication – username,password(postman)

DockerFile

A docker file is created within the project folder LogPorxyApi 

Steps to create and run container:
•	Publish the project to bin/Release/netcoreapp3.1/
•	Navigate to the project folder (LogProxyApi/DockerFile.txt) in terminal.
•	Build the project using the command d docker build -t logproxyapi -f Dockerfile .
•	Run the docker container using docker run -d -p 8080:80 --name myapp logproxyapi
•	Use the urls http://localhost:8080/logproxy/GetLog,   http://loglhost:8080/logproxy/PostLog 

