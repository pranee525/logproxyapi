using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogProxyApi.Controllers;
using LogProxyApi.Services;
using LogProxyApi.Models;
using AirtableApiClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Moq;
namespace LogProxyTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task getRecords()
        {
            airTableLog aLog = new airTableLog();
            var airRecord = await aLog.getAllRecordsAsync(3);
            Assert.IsNotNull(airRecord);
        }

        [TestMethod]
        [DataRow("Exception Summary 1", "exception at")]
        [DataRow("Exception Summary 2", "exception at")]
        [DataRow("Exception Summary 3", "exception at")]
        public async Task postRecords(string text, string title)
        {
            airTableLog aLog = new airTableLog();
            var ariRecord = await aLog.PostAllRecords(text, title);
            Assert.IsNotNull(ariRecord);
        }

        [TestMethod]
        [DataRow("test", "test", "test")]
        [DataRow("test_sample", "test", "test_sample")]
        [DataRow("test_fail", "testfail", "")]
        public async Task authenticateUser(string userName, string password, string result)
        {
            UserService uService = new UserService();
            var userData = await uService.Authenticate(userName, password);
            if (userData != null)
            {
                Assert.AreEqual(userData.Username, result);
            }
            else
            {
                Assert.IsNull(userData);
            }
        }

        [TestMethod]
        [DataRow("test", "test", 3)]
        [DataRow("test_sample", "test", 3)]
        public async Task ControllerTest(string userName, string password, int maxRecords)
        {

            var mock = new Mock<IUserService>();
            mock.Setup(p => p.Authenticate(userName, password));

            var logmock = new Mock<ILogProxy>();
            logmock.Setup(p => p.GetLogs(maxRecords));
            LogProxyController logController = new LogProxyController(mock.Object, logmock.Object);
            var records = logController.GetLogs(maxRecords);

            Assert.IsNotNull(records);


        }


    }


}
