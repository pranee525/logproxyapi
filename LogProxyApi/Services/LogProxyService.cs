﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogProxyApi.Models;
using LogProxyApi.Helpers;
using AirtableApiClient;
namespace LogProxyApi.Services
{
    public interface ILogProxy
    {
        Task<List<AirtableRecord>> GetLogs(int maxRecords);
        Task<AirtableRecord> PostLogs(string text, string title);
    }

    public class LogProxyService : ILogProxy
    {
        public async Task<List<AirtableRecord>> GetLogs(int maxRecords)
        {
            airTableLog aLog = new airTableLog();
            List<AirtableRecord> lst_records = new List<AirtableRecord>();

            lst_records = await aLog.getAllRecordsAsync(maxRecords);
            //lst_records.records = aList;
            return lst_records;
        }
        public async Task<AirtableRecord> PostLogs(string text, string titles)
        {
            airTableLog aLog = new airTableLog();

            AirtableRecord aList = new AirtableRecord();
            aList = await aLog.PostAllRecords(text, titles);

            return aList;
        }
    }

}
