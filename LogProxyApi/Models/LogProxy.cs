using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.Xml;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using AirtableApiClient;

namespace LogProxyApi.Models
{
    //To log the airtable data
    public class airTableLog
    {
        public string id { get; set; }

        public logFields fields { get; set; }
        public DateTime createdTime { get; set; }
        //airtable api key
        private string apiToken = "key46INqjpp7lMzjd";
        //airtable base id
        private string baseId = "appD1b1YjWoXkUJwR";


        //gets data from airtable api
        public async Task<List<AirtableRecord>> getAllRecordsAsync(int maximumRecords)
        {
            var airRecord = new List<AirtableRecord>();

            using (AirtableBase airtableBase = new AirtableBase(apiToken, baseId))
            {

                Task<AirtableListRecordsResponse> task = airtableBase.ListRecords("Messages", maxRecords: maximumRecords, view: "Grid view");
                var response = await task;

                if (!response.Success)
                {
                    string errorMessage = null;
                    if (response.AirtableApiError is AirtableApiException)
                    {
                        errorMessage = response.AirtableApiError.ErrorMessage;
                    }
                    else
                    {
                        errorMessage = "Unknown error";
                    }
                }
                else
                {
                    airRecord = response.Records.ToList();
                }
            }
            return airRecord;
        }

        //posts the data to airtable api
        public async Task<AirtableRecord> PostAllRecords(string text, string title)
        {
            var lstAirTable = new List<airTableLog>();
            var airRecord = new AirtableRecord();

            using (AirtableBase airtableBase = new AirtableBase(apiToken, baseId))
            {

                var fields = new Fields();
                fields.AddField("Message", title);
                fields.AddField("Summary", text);

                fields.AddField("receivedAt", DateTime.Now);


                Task<AirtableCreateUpdateReplaceRecordResponse> task = airtableBase.CreateRecord("Messages", fields, true);
                var response = await task;

                if (!response.Success)
                {
                    string errorMessage = null;
                    if (response.AirtableApiError is AirtableApiException)
                    {
                        errorMessage = response.AirtableApiError.ErrorMessage;
                    }
                    else
                    {
                        errorMessage = "Unknown error";
                    }
                }
                else
                {
                    airRecord = response.Record;
                }
            }
            return airRecord;
        }

    }

    public class logFields
    {
        public string id { get; set; }
        public string Message { get; set; }
        public DateTime receivedAt { get; set; }
        public string Summary { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }



}
