﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LogProxyApi.Models;
using Microsoft.AspNetCore.Authorization;
using LogProxyApi.Services;

namespace LogProxyApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LogProxyController : ControllerBase
    {

        private IUserService _userService;
        private ILogProxy _proxyService;

        public LogProxyController(IUserService userService, ILogProxy proxyService)
        {
            _userService = userService;
            _proxyService = proxyService;
        }

        //Takes the maxRecords as input and return those records as outpu
        [HttpGet("{apiname}", Name = "GetLog")]
        public async Task<IActionResult> GetLogs(int maxRecords)
        {
            if (maxRecords == 0)
            {
                return BadRequest();
            }
            else
            {
                var aList = await _proxyService.GetLogs(maxRecords);

                return Ok(aList);
            }
        }
        //post the data to airtable ,test is the summary and title is the message
        //returns bad request incase of parameter mismatch
        [HttpPost("{apiname}", Name = "PostLog")]
        public async Task<IActionResult> PostLogs(string text, string title)
        {
            if (text != null && title != null)
            {
                var aList = await _proxyService.PostLogs(text, title);

                return Ok(aList);
            }
            else
            {
                return BadRequest();
            }
        }

    }
}
