﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogProxyApi.Models;

namespace LogProxyApi.Helpers
{
    public static class ExtensionMethods
    {
        //returns the userinfo without password
        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            user.Password = null;
            return user;
        }
    }
}
